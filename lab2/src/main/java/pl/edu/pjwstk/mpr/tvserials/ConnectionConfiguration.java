package pl.edu.pjwstk.mpr.tvserials;

import java.sql.Connection;
import java.sql.DriverManager;


public class ConnectionConfiguration {

    public static Connection getConnection(){
        Connection connection = null;

        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost:9001/","SA","");

        }catch(Exception e){
            e.getStackTrace();
        }
        return connection;
    }
}
